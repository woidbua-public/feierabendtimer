﻿using System;
using System.Windows;
using FeierabendTimer.ViewModels;
using FeierabendTimer.Views;

namespace FeierabendTimer.Services
{
    public class PopupService : IPopupService
    {
        public MessageBoxResult ShowMessageBox(string text, string title, MessageBoxButton buttons, MessageBoxImage icon = MessageBoxImage.Asterisk, MessageBoxResult defaultResult = MessageBoxResult.None)
        {
            throw new NotImplementedException();
        }

        public void ShowInfoPopup()
        {
            var popup = new InfoPopupWindow
            {
                Owner = Application.Current.MainWindow,
                DataContext = new InfoPopupViewModel()
            };

            popup.ShowDialog();
        }
    }
}