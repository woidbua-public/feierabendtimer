﻿using System.Windows;

namespace FeierabendTimer.Services
{
    public interface IPopupService
    {
        MessageBoxResult ShowMessageBox(string text, string title, MessageBoxButton buttons, MessageBoxImage icon = MessageBoxImage.Information, MessageBoxResult defaultResult = MessageBoxResult.None);

        void ShowInfoPopup();
    }
}