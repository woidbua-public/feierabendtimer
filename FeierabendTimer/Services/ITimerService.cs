﻿using System;

namespace FeierabendTimer.Services
{
    public interface ITimerService
    {
        bool IsRunning();

        void Start(DateTime endTime, Action<TimeSpan> action);

        void Stop();
    }
}