﻿using System;
using System.Windows.Threading;

namespace FeierabendTimer.Services
{
    public class DispatchTimerService : ITimerService
    {
        private DispatcherTimer _timer;

        private DateTime _endTime;
        private Action<TimeSpan> _action;


        public void Start(DateTime endTime, Action<TimeSpan> action)
        {
            if (_timer != null) Stop();

            _endTime = SetupEndTime(endTime);
            _action = action;
            _action(CalculateRemainingDuration());

            _timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(200)
            };

            _timer.Tick += TimerTick;
            _timer.Start();
        }


        public bool IsRunning()
        {
            return _timer != null && _timer.IsEnabled;
        }


        public void Stop()
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Tick -= TimerTick;
            }

            _timer = null;
        }

        private static DateTime SetupEndTime(DateTime endTime)
        {
            var currentTime = DateTime.Now;
            var duration = endTime.Subtract(currentTime);
            return duration < TimeSpan.Zero ? endTime.AddDays(1) : endTime;
        }


        private void TimerTick(object sender, EventArgs e)
        {
            _action(CalculateRemainingDuration());
        }

        private TimeSpan CalculateRemainingDuration()
        {
            var currentTime = DateTime.Now;
            var rawDuration = _endTime.Subtract(currentTime);
            return TimeSpan.FromSeconds(Math.Round(rawDuration.TotalSeconds));
        }
    }
}