﻿namespace FeierabendTimer.Services
{
    public interface IDialogService
    {
        void ShowMessage(string title, string message);
    }
}