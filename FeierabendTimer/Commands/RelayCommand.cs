﻿using System;
using System.Windows.Input;

namespace FeierabendTimer.Commands
{
    public class RelayCommand : RelayCommand<object>
    {
        public RelayCommand(Action execute, Func<bool> canExecute = null)
            : base(p => execute(), p => canExecute?.Invoke() ?? true)
        {
        }
    }

    public class RelayCommand<TParameter> : ICommand
    {
        private readonly Func<TParameter, bool> _canExecute;
        private readonly Action<TParameter> _execute;


        public RelayCommand(Action<TParameter> execute, Func<TParameter, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute ?? (p => true);
        }


        public bool CanExecute(object parameter)
        {
            return _canExecute((TParameter) parameter);
        }

        public void Execute(object parameter)
        {
            _execute((TParameter) parameter);
        }


        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}