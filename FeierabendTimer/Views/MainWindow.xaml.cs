﻿using System;
using System.ComponentModel;
using System.Windows;
using FeierabendTimer.Properties;
using FeierabendTimer.Services;
using FeierabendTimer.ViewModels;

namespace FeierabendTimer.Views
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel(new DispatchTimerService(), new PopupService());
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            Settings.Default.ClockHour = int.Parse(HourTextBox.Text);
            Settings.Default.ClockMinute = int.Parse(MinuteTextBox.Text);
            Settings.Default.Save();
            base.OnClosing(e);
        }
    }
}