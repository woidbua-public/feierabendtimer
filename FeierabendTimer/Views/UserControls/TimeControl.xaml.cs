﻿using System.Windows;
using System.Windows.Controls;


namespace FeierabendTimer.Views.UserControls
{
    /// <summary>
    ///     Interaction logic for TimeControl.xaml
    /// </summary>
    public partial class TimeControl : UserControl
    {
        public static readonly DependencyProperty ClockHourProperty = DependencyProperty.Register(
            "ClockHour",
            typeof(string),
            typeof(TimeControl),
            new PropertyMetadata("0")
        );

        public static readonly DependencyProperty ClockMinuteProperty = DependencyProperty.Register(
            "ClockMinute",
            typeof(string),
            typeof(TimeControl),
            new PropertyMetadata("0")
        );


        public TimeControl()
        {
            InitializeComponent();
        }


        public string ClockHour
        {
            get => (string) GetValue(ClockHourProperty);
            set => SetValue(ClockHourProperty, value);
        }

        public string ClockMinute
        {
            get => (string) GetValue(ClockMinuteProperty);
            set => SetValue(ClockMinuteProperty, value);
        }
    }
}