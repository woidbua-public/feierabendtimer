﻿using System.Windows;
using System.Windows.Input;

namespace FeierabendTimer.Views
{
    /// <summary>
    ///     Interaction logic for InfoPopupWindow.xaml
    /// </summary>
    public partial class InfoPopupWindow : Window
    {
        public InfoPopupWindow()
        {
            InitializeComponent();
        }

        private void CloseCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }
    }
}