﻿using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;


namespace FeierabendTimer.Views.CustomControls
{
    public class NumberTextBox : TextBox
    {
        public NumberTextBox()
        {
            PreviewTextInput += OnTextInput;
            PreviewKeyDown += OnPreviewKeyDown;
        }


        private void OnTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(c => !char.IsDigit(c)))
            {
                e.Handled = true;
            }
        }


        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
    }
}