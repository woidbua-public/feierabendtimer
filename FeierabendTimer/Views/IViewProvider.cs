﻿using System.Windows;

namespace FeierabendTimer.Views
{
    public interface IViewProvider
    {
        Window View { get; }
    }
}