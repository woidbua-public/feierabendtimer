﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;

namespace FeierabendTimer.Behaviors
{
    public class SelectAllFocusBehavior : Behavior<TextBox>
    {
        public static readonly DependencyProperty EnableProperty = DependencyProperty.RegisterAttached(
            "Enable",
            typeof(bool),
            typeof(SelectAllFocusBehavior),
            new FrameworkPropertyMetadata(false, OnEnableChanged)
        );

        public static bool GetEnable(Control control)
        {
            return (bool) control.GetValue(EnableProperty);
        }

        private static void IgnoreMouseButton(object sender, MouseButtonEventArgs e)
        {
            var control = sender as Control;
            if (control == null || control.IsKeyboardFocusWithin) return;
            e.Handled = true;
            control.Focus();
        }

        private static void OnEnableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as Control;
            if (control == null) return;

            if (e.NewValue is bool == false) return;

            if ((bool) e.NewValue)
            {
                control.GotFocus += SelectAll;
                control.PreviewMouseDown += IgnoreMouseButton;
            }
            else
            {
                control.GotFocus -= SelectAll;
                control.PreviewMouseDown -= IgnoreMouseButton;
            }
        }

        private static void SelectAll(object sender, RoutedEventArgs e)
        {
            var control = e.OriginalSource as Control;
            if (control is TextBox)
                ((TextBoxBase) control).SelectAll();
        }

        public static void SetEnable(Control control, bool value)
        {
            control.SetValue(EnableProperty, value);
        }
    }
}