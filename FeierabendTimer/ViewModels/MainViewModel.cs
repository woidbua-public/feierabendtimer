﻿using System;
using FeierabendTimer.Commands;
using FeierabendTimer.Properties;
using FeierabendTimer.Services;

namespace FeierabendTimer.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IPopupService _popupService;
        private readonly ITimerService _timerService;

        private int _clockHour = Settings.Default.ClockHour;
        private int _clockMinute = Settings.Default.ClockMinute;

        private TimeSpan _remainingDuration = TimeSpan.Zero;

        private RelayCommand _startTimerCommand;
        private RelayCommand _stopTimerCommand;


        public MainViewModel(ITimerService timerService, IPopupService popupService)
        {
            _timerService = timerService;
            _popupService = popupService;
        }


        public int ClockHour
        {
            get => _clockHour;
            set
            {
                if (_clockHour == value)
                    return;

                if (value < 0)
                    _clockHour = 0;
                else if (value > 23)
                    _clockHour = 23;
                else
                    _clockHour = value;

                OnPropertyChanged();
            }
        }


        public int ClockMinute
        {
            get => _clockMinute;
            set
            {
                if (_clockMinute == value)
                    return;

                if (value < 0)
                    _clockMinute = 0;
                else if (value > 59)
                    _clockMinute = 59;
                else
                    _clockMinute = value;

                OnPropertyChanged();
            }
        }

        public bool IsRunning => _timerService.IsRunning();


        public TimeSpan RemainingDuration
        {
            get => _remainingDuration;
            private set
            {
                if (_remainingDuration == value)
                    return;

                _remainingDuration = value;
                OnPropertyChanged();
            }
        }


        public RelayCommand StartTimerCommand =>
            _startTimerCommand ??= new RelayCommand(StartTimer, CanStartTimer);

        public RelayCommand StopTimerCommand =>
            _stopTimerCommand ??= new RelayCommand(StopTimer, CanStopTimer);


        private bool CanStartTimer()
        {
            return !IsRunning;
        }

        private bool CanStopTimer()
        {
            return IsRunning;
        }


        private void StartTimer()
        {
            var endTime = DateTime.Parse($"{ClockHour}:{ClockMinute}");
            _timerService.Start(endTime, UpdateRemainingDuration);
            OnPropertyChanged(nameof(IsRunning));
        }


        private void StopTimer()
        {
            _timerService.Stop();
            RemainingDuration = TimeSpan.Zero;
            OnPropertyChanged(nameof(IsRunning));
        }


        private void UpdateRemainingDuration(TimeSpan duration)
        {
            if (duration.TotalSeconds > 0)
            {
                RemainingDuration = duration;
            }
            else
            {
                StopTimerCommand.Execute(null);
                _popupService.ShowInfoPopup();
            }
        }
    }
}