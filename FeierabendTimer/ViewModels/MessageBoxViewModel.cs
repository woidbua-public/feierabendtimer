﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using FeierabendTimer.Commands;

namespace FeierabendTimer.ViewModels
{
    internal interface IRequestClose
    {
        event EventHandler CloseRequested;
    }

    internal class MessageBoxViewModel : BaseViewModel, IRequestClose
    {
        public MessageBoxViewModel(string text, string title, MessageBoxButton buttons, MessageBoxImage icon, MessageBoxResult defaultResult)
        {
            Message = text;
            Title = title;
            Icon = icon;
            DefaultResult = defaultResult;

            Commands = new ObservableCollection<MessageBoxCommandViewModel>(ConstructCommands(buttons, defaultResult));
            CloseCommand = new RelayCommand<MessageBoxResult>(Close);
        }

        public ICommand CloseCommand { get; }

        public IEnumerable<MessageBoxCommandViewModel> Commands { get; }

        public MessageBoxResult DefaultResult { get; private set; }


        public MessageBoxImage Icon { get; }

        public string Message { get; }

        public string Title { get; }

        private void Close(MessageBoxResult result)
        {
            DefaultResult = result;
            RequestClose();
        }

        private IEnumerable<MessageBoxCommandViewModel> ConstructCommands(MessageBoxButton buttons, MessageBoxResult defaultResult)
        {
            if (HasButton(buttons, MessageBoxResult.OK))
                yield return new MessageBoxCommandViewModel("Ok", MessageBoxResult.OK, defaultResult == MessageBoxResult.OK, false);

            if (HasButton(buttons, MessageBoxResult.Cancel))
                yield return new MessageBoxCommandViewModel("Cancel", MessageBoxResult.Cancel, defaultResult == MessageBoxResult.Cancel, true);

            if (HasButton(buttons, MessageBoxResult.Yes))
                yield return new MessageBoxCommandViewModel("Yes", MessageBoxResult.Yes, defaultResult == MessageBoxResult.Yes, false);

            if (HasButton(buttons, MessageBoxResult.No))
                yield return new MessageBoxCommandViewModel("No", MessageBoxResult.No, defaultResult == MessageBoxResult.No, true);
        }

        private static bool HasButton(MessageBoxButton buttons, MessageBoxResult result)
        {
            return result switch
            {
                MessageBoxResult.OK => buttons == MessageBoxButton.YesNo || buttons == MessageBoxButton.YesNoCancel,
                MessageBoxResult.Yes => buttons == MessageBoxButton.YesNo || buttons == MessageBoxButton.YesNoCancel,
                MessageBoxResult.No => buttons == MessageBoxButton.YesNo || buttons == MessageBoxButton.YesNoCancel,
                MessageBoxResult.Cancel => buttons == MessageBoxButton.OKCancel || buttons == MessageBoxButton.YesNoCancel,
                _ => false
            };
        }

        private void RequestClose()
        {
            CloseRequested?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler CloseRequested;
    }

    internal class MessageBoxCommandViewModel : BaseViewModel
    {
        public MessageBoxCommandViewModel(string text, MessageBoxResult result, bool isDefault, bool isCancel)
        {
            Text = text;
            Result = result;
            IsDefault = isDefault;
            IsCancel = isCancel;
        }


        public bool IsCancel { get; }

        public bool IsDefault { get; }

        public MessageBoxResult Result { get; }

        public string Text { get; }
    }
}