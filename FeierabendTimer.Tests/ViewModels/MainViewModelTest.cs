﻿using System;
using FeierabendTimer.Services;
using FeierabendTimer.ViewModels;
using NSubstitute;
using NUnit.Framework;

namespace FeierabendTimer.Tests.ViewModels
{
    [TestFixture]
    public class MainViewModelTest
    {
        [Test]
        public void ClockHourShouldBeSetToMaxValueWhenInputGreaterThanMaxValue()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();
            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.ClockHour = 50;

            // Assert
            Assert.AreEqual(23, sut.ClockHour);
        }


        [Test]
        public void ClockHourShouldBeSetToMinValueWhenInputLessThanMinValue()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();
            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.ClockHour = -10;

            // Assert
            Assert.AreEqual(0, sut.ClockHour);
        }


        [Test]
        public void ClockHourShouldBeSetToValueWhenInputInValueRange()
        {
            // Arrange
            const int expected = 13;
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();
            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.ClockHour = expected;

            // Assert
            Assert.AreEqual(expected, sut.ClockHour);
        }

        [Test]
        public void ClockHourShouldHaveDefaultValue()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();

            // Act
            var sut = new MainViewModel(timerService, popupService);

            // Assert
            Assert.AreEqual(16, sut.ClockHour);
        }


        [Test]
        public void ClockMinuteShouldBeSetToMaxValueWhenInputGreaterThanMaxValue()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();
            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.ClockMinute = 100;

            // Assert
            Assert.AreEqual(59, sut.ClockMinute);
        }


        [Test]
        public void ClockMinuteShouldBeSetToMinValueWhenInputLessThanMinValue()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();
            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.ClockMinute = -10;

            // Assert
            Assert.AreEqual(0, sut.ClockMinute);
        }


        [Test]
        public void ClockMinuteShouldBeSetToValueWhenInputInValueRange()
        {
            // Arrange
            const int expected = 13;
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();
            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.ClockMinute = expected;

            // Assert
            Assert.AreEqual(expected, sut.ClockMinute);
        }


        [Test]
        public void ClockMinuteShouldHaveDefaultValue()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();

            // Act
            var sut = new MainViewModel(timerService, popupService);

            // Assert
            Assert.AreEqual(0, sut.ClockMinute);
        }


        [Test]
        public void RemainingDurationShouldBeResetWhenTimerIsStopped()
        {
            // Arrange
            var duration = TimeSpan.FromSeconds(444);

            var timerService = Substitute.For<ITimerService>();
            timerService.Start(Arg.Any<DateTime>(), Arg.Invoke(duration));
            var popupService = Substitute.For<IPopupService>();

            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.StartTimerCommand.Execute(null);
            sut.StopTimerCommand.Execute(null);

            // Assert
            Assert.AreEqual(TimeSpan.Zero, sut.RemainingDuration);
        }


        [Test]
        public void RemainingDurationShouldBeSetWhenTimerTicks()
        {
            // Arrange
            var expected = TimeSpan.FromSeconds(444);

            var timerService = Substitute.For<ITimerService>();
            timerService.Start(Arg.Any<DateTime>(), Arg.Invoke(expected));
            var popupService = Substitute.For<IPopupService>();

            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.StartTimerCommand.Execute(null);

            // Assert
            Assert.AreEqual(expected, sut.RemainingDuration);
        }

        [Test]
        public void ShowMessageShouldBeCalledWhenRemainingDurationIsZero()
        {
            // Arrange
            var expected = TimeSpan.Zero;

            var timerService = Substitute.For<ITimerService>();
            timerService.Start(Arg.Any<DateTime>(), Arg.Invoke(expected));
            var popupService = Substitute.For<IPopupService>();

            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.StartTimerCommand.Execute(null);

            // Assert
            Assert.AreEqual(expected, sut.RemainingDuration);
            popupService.Received().ShowInfoPopup();
        }


        [Test]
        public void StartTimerShouldBeCalledWhenStartCommandExecuted()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            var popupService = Substitute.For<IPopupService>();

            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.StartTimerCommand.Execute(null);

            // Assert
            timerService.Received().Start(Arg.Any<DateTime>(), Arg.Any<Action<TimeSpan>>());
        }


        [Test]
        public void StartTimerShouldBeExecutableWhenTimerIsNotRunning()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            timerService.IsRunning().Returns(false);
            var popupService = Substitute.For<IPopupService>();

            // Act
            var sut = new MainViewModel(timerService, popupService);

            // Assert
            Assert.IsTrue(sut.StartTimerCommand.CanExecute(null));
        }


        [Test]
        public void StartTimerShouldNotBeExecutableWhenTimerIsRunning()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            timerService.IsRunning().Returns(true);
            var popupService = Substitute.For<IPopupService>();

            // Act
            var sut = new MainViewModel(timerService, popupService);

            // Assert
            Assert.IsFalse(sut.StartTimerCommand.CanExecute(null));
        }

        [Test]
        public void StopTimerShouldBeCalledWhenRemainingDurationIsZero()
        {
            // Arrange
            var expected = TimeSpan.Zero;

            var timerService = Substitute.For<ITimerService>();
            timerService.Start(Arg.Any<DateTime>(), Arg.Invoke(expected));
            var popupService = Substitute.For<IPopupService>();

            var sut = new MainViewModel(timerService, popupService);

            // Act
            sut.StartTimerCommand.Execute(null);

            // Assert
            Assert.AreEqual(expected, sut.RemainingDuration);
            timerService.Received().Stop();
        }


        [Test]
        public void StopTimerShouldBeExecutableWhenTimerIsRunning()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            timerService.IsRunning().Returns(true);
            var popupService = Substitute.For<IPopupService>();

            // Act
            var sut = new MainViewModel(timerService, popupService);

            // Assert
            Assert.IsTrue(sut.StopTimerCommand.CanExecute(null));
        }


        [Test]
        public void StopTimerShouldNotBeExecutableWhenTimerIsNotRunning()
        {
            // Arrange
            var timerService = Substitute.For<ITimerService>();
            timerService.IsRunning().Returns(false);
            var popupService = Substitute.For<IPopupService>();

            // Act
            var sut = new MainViewModel(timerService, popupService);

            // Assert
            Assert.IsFalse(sut.StopTimerCommand.CanExecute(null));
        }
    }
}